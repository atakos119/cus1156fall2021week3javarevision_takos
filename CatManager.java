package cus1156.catlab2;

import java.util.ArrayList;

/**
 * This class represents a group of cats. The cats are created when the CatManager is created
 * although more cats can be added later.
 */

public class CatManager {

	private ArrayList <Cat> myCats; // ArrayList data structure of all cats

	public CatManager(ArrayList <Cat> myCats) {
		myCats = this.myCats;
	}
	
	public CatManager()
	{
		myCats = new ArrayList<>();
		Cat cat = new Cat("Fifi", "black");
		myCats.add(cat);
		cat = new Cat("Fluffy", "spotted");
		myCats.add(cat);
		cat = new Cat("Josephine", "tabby");
		myCats.add(cat);
		cat = new Cat("Biff", "tabby");
		myCats.add(cat);
		cat = new Cat("Bumpkin", "white");
		myCats.add(cat);
		cat = new Cat("Spot", "spotted");
		myCats.add(cat);
		cat = new Cat("Lulu", "tabby");
		myCats.add(cat);
	}


	/** add a cat to the group
	 * @param name
	 * @return
	 */

	public void add(Cat cat)
	{
		myCats.add(cat);
	}


	/**returns the first cat whose name matches, or null if no cat
                    // with that name is found
	 * 
	 * @param name - the name of the Cat that we are searching for
	 * @return the Cat object or null if not found
	 */
	public Cat findThisCat(String name)
	{	
		for(Cat cat: myCats)
		{
			if(cat.getName().equals(name))
			{
				return cat;
			}            

		} // for-loop ends here 

		return null;

	} // <--- findThisCat() method ends here


	// returns the number of cats of this color
	/**
	 * returns the number of cats of this color
	 * @param color - the color we are counting
	 * @return - the number of cats
	 */
	
	public int countColors(String color)
	{   
		int numCats = 0;        
		for(Cat cat: myCats)
		{
			if(cat.getColor().equals(color))
			{
				numCats++;
			}
			
			else
			{
				numCats = 0;
			}

		} // <--- for-loop ends here

		return numCats;

	} // <--- countColors() method ends here

	
} // <--- CatManager class ends here
