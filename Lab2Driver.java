package cus1156.catlab2;

import java.util.Scanner;
import java.util.ArrayList;

public class Lab2Driver {
	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		CatManager catMgr = new CatManager();

		int numTabby = catMgr.countColors("tabby");
		int numSpotted = catMgr.countColors("spotted");
		System.out.println("There are " + numTabby + " tabby cats");
		System.out.println("There are " + numSpotted + " spotted cats");



		// Added section to utilize add() method:			
		String new_cat_name = "Steve";
		String new_cat_color = "white";
		
		Cat new_cat = new Cat(new_cat_name, new_cat_color);
		catMgr.add(new_cat);
		System.out.println("You have added a new cat named " + new_cat_name + " with the color " + new_cat.getColor());


		System.out.println("Enter the name of the cat you would like to find");
		String name =  input.next();
		Cat foundCat = catMgr.findThisCat(name);
		if (foundCat == null)
			System.out.println("did not find this cat");
		else
			foundCat.manyMeows(3);
		

		System.out.println(); // <--- line break

		input.close();

	} // <--- main() method ends here

} // <--- Lab2Driver class ends here
